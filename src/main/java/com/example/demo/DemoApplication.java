package com.example.demo;

import java.io.File;
import java.net.URISyntaxException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws URISyntaxException {
		ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class);
		File csv = new File(context.getClass().getResource("/application.properties").toURI());
		System.out.println(csv.getAbsolutePath());
		System.out.println(String.format("does file exists? %s", csv.exists()));
	}
}
